# WordPress Security Generator
## No dependencies! 1 file!
---
### Generates necessary important code snippets such as:

- Htpasswd file (username and hashed password)
- ability to have custom password
- Protecting wp-login.php 
- Protecting include-only files
- Protecting wp-config.php file
- Password Protect the /wp-admin directory
- Hide login error messages function
- Suggested list of WP Plugins
- and more to come!

## Installation
It's easy to install! Place wp-security.php in the root directory of your WordPress build. It should be on the same level as wp-config.php.

## Generate custom password

You can easily add your own custom password without editing the wp-security.php file. Add the `?custom=custom_password` to your URL, so it looks like this:
`http://www.yoururl.com/wp-security.php?custom=custom_password_here` 
The script will use that password instead of generating a random one.

### You can increase the length of the randomly generated password but you must edit the wp-security.php file. Go to line 15 and edit the $passwordLength variable. 

## Current Version: 1.0.1
- Added stronger password generation depending on PHP version number

### 1.0.0
- Initial version
