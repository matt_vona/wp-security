<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Wordpress Security Generator</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <?php
        /* FUNCTIONS */ 
        function randomPassword() {
            $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
            $pass = array(); //remember to declare $pass as an array
            $passwordLength = 10;
            $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
            for ($i = 0; $i < $passwordLength; $i++) {
                $n = rand(0, $alphaLength);
                $pass[] = $alphabet[$n];
            }
            return implode($pass); //turn the array into a string
        }

        function generatePassword($p) {
            // Compare PHP Version to check which hasing algorithm it can use
            if (version_compare(PHP_VERSION, '5.5.0') >= 0 && function_exists('password_hash')) {
                $password = password_hash($p, PASSWORD_DEFAULT);
            } else {
                // Encrypt password, fallback for php v < 5.5.0
                $password = crypt($p, base64_encode(randomPassword()));
            }          

            // Print encrypted password
            return $password;
        }

        function getPassword() {
            if (!isset($_GET['custom'])) { 
                return randomPassword(); 
            } else {
                return $_GET['custom'];
            };
        }
        
        $username = "admin";
        $password = getPassword();
        $hashed = generatePassword($password);

        ?>

        <style>
          body {
            font-family: helvetica, arial, sans-serif;
          }
          .alert{
            color: #E29595;
            text-transform: uppercase;
            border: 1px #ccc solid;
            padding: 5px;
            margin-bottom: 0;
            border-radius: 4px;
            position: fixed;
            top: 10px;
            background: #fff;
          }
          header {
            margin-top: 80px;
          }
          .container {
            width: 800px;
            max-width: 100%;
            margin: 0 auto;
          }
          .row {
            margin-bottom: 30px;
            clear: both;
            display: block;
          }
          .half {
            width: 100%;
          }
          .half:first-child {
            margin-right: 0;
          }
          .third {
            width: 100%;
          }
          .third:first-child {
            margin-right: 0;
          }
          .third:last-child {
            margin-left: 0;
          }
          textarea,
          input[type="text"] {
            border-radius: 4px;
            border: solid 1px #aaa;
            width: 98%;
            font-family: monospace;
            padding: 5px;
          }

          @media only screen and (min-Width: 768px) {
            .row {
              display: flex;
            }
            .half {
              width: 48%;
            }
            .third {
              width: 31%;
            }
            .half:first-child,
            .third:first-child {
              margin-right: 2%;
            }
            .third:last-child {
              margin-left: 2%;
            }
          }
        </style>

    </head>
    <body>
        <div class="container">
            <h3 class="alert">Please Remember to delete this file!</h3>
        </div>
        <header>
            <section class="container">
              <h2 class="logo">WordPress Security Checklist</h2>
            </section>
        </header>
        
        <section class="container">
            <p>Htaccess and Password</p>
            <div class="row">
                <div class="third">
                    <label for="">Username</label>  
                    <input type="text" value="<?php echo $username; ?>"/>
                </div>
                <div class="third">
                    <label for="">Password</label>
                    <input type="text" value="<?php echo $password; ?>"/>
                </div>
                <div class="third">
                    <label for="">Encrypted Password</label>
                    <input type="text" value="<?php echo $hashed; ?>">
                </div>
            </div>
            <p>.htpasswd: <br />
            Suggested location: <?php echo dirname(__DIR__); ?>/.htpasswd</p>
            <div class="row">
              <div class="half">
                <input type="text" name="" id="" value="<?php echo $username.":".$hashed; ?>">
              </div>
            </div>

            <h2>Wp-config.php</h2>
            <div class="row">
                <div class="half">
                    <p>Disable wordpress theme editor (/wp-config.php)</p>
                    <input type="text" value="define('DISALLOW_FILE_EDIT', true);"/>
                </div>                
            </div>

            <h2>WP Root .htaccess</h2>
            <div class="row">
                <div class="half">
                    <p>Password Protect wp-login.php <a href="http://www.htaccesstools.com/htpasswd-generator/">(Htpasswd Generator)</a></p>
                    <textarea name="" id="" cols="30" rows="10">
# protect wp-login.php
<files wp-login.php>
AuthName "Login Area"
AuthType Basic
AuthUserFile <?php echo dirname(__DIR__); ?>/.htpasswd
require valid-user
</files>
                    </textarea>
                </div>
            </div>
            <div class="row">
                <div class="half">
                    <p>Block the include-only files</p>
                    <textarea name="" id="" cols="30" rows="10">
# Block the include-only files.
<IfModule mod_rewrite.c>
RewriteEngine On
RewriteBase /
RewriteRule ^wp-admin/includes/ - [F,L]
RewriteRule !^wp-includes/ - [S=3]
RewriteRule ^wp-includes/[^/]+\.php$ - [F,L]
RewriteRule ^wp-includes/js/tinymce/langs/.+\.php - [F,L]
RewriteRule ^wp-includes/theme-compat/ - [F,L]
</IfModule>
                    </textarea>
                </div>
                <div class="half">
                    <p>Block the wp-config.php file</p>
                    <textarea name="" id="" cols="30" rows="10">
# Block wp-config.php file
<files wp-config.php>
order allow,deny
deny from all
</files>
                    </textarea>
                </div>
            </div>

            <h2>/wp-admin htaccess (/wp-admin/.htaccess)</h2>
            <p>Suggested htaccess location: <?php echo dirname(__DIR__); ?> <br />
            Yours might be in a different place.</p>
            <div class="row">
              <div class="half">
                    <p>Password Protect /wp-admin</p>
                    <textarea name="" id="" cols="30" rows="10">
AuthType Basic
AuthName "restricted area"
AuthUserFile <?php echo dirname(__DIR__); ?>/.htpasswd
require valid-user

# Allows for admin ajax calls
<Files admin-ajax.php>
    Order allow,deny
    Allow from all
    Satisfy any
</Files>
                    </textarea>
                </div>
            </div>

            <h2>Functions.php</h2>
            <div class="row">
                <div class="half">
                    <p>Hide login error messages (functions.php)</p>
                    <textarea name="" id="" cols="30" rows="10">
function error_message_failed_login() {
    return 'The username or password is incorrect.';
}
         
add_filter('login_errors', 'error_message_failed_login');
                    </textarea>
                </div>
            </div>
            <!-- <div class="row">
                <div class="half">
                    
                </div>
                <div class="half">
                    
                </div>
            </div> -->
        </section>
        

        <section class="container">
            <h2>Plugins</h2>

            <ul>
                <li>Block Bad Queries</li>
                <li>Wordfence</li>
                <li>akismet (prevents spam)</li>
            </ul>
        </section>
    </body>
</html>